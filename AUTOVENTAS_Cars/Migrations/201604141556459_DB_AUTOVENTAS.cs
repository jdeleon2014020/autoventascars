namespace AUTOVENTAS_Cars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DB_AUTOVENTAS : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Usuarios", "rol_idRol", "dbo.Rols");
            DropIndex("dbo.Usuarios", new[] { "rol_idRol" });
            RenameColumn(table: "dbo.Archivoes", name: "vehiculo_idVehiculo", newName: "idVehiculo");
            RenameColumn(table: "dbo.Usuarios", name: "rol_idRol", newName: "idRol");
            RenameIndex(table: "dbo.Archivoes", name: "IX_vehiculo_idVehiculo", newName: "IX_idVehiculo");
            AlterColumn("dbo.Usuarios", "idRol", c => c.Int(nullable: false));
            CreateIndex("dbo.Usuarios", "idRol");
            AddForeignKey("dbo.Usuarios", "idRol", "dbo.Rols", "idRol", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Usuarios", "idRol", "dbo.Rols");
            DropIndex("dbo.Usuarios", new[] { "idRol" });
            AlterColumn("dbo.Usuarios", "idRol", c => c.Int());
            RenameIndex(table: "dbo.Archivoes", name: "IX_idVehiculo", newName: "IX_vehiculo_idVehiculo");
            RenameColumn(table: "dbo.Usuarios", name: "idRol", newName: "rol_idRol");
            RenameColumn(table: "dbo.Archivoes", name: "idVehiculo", newName: "vehiculo_idVehiculo");
            CreateIndex("dbo.Usuarios", "rol_idRol");
            AddForeignKey("dbo.Usuarios", "rol_idRol", "dbo.Rols", "idRol");
        }
    }
}
