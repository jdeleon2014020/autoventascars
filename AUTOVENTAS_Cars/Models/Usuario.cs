﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AUTOVENTAS_Cars.Models
{
    public class Usuario
    {
        [Key]
        public int idUsuario { get; set; }

        [Display(Name = "Nombre"), Required(ErrorMessage = "Campo Obligatorio")]
        public String nombre { get; set; }

        [Display(Name = "Correo"), Required(ErrorMessage = "Campo Obligatorio"), DataType(DataType.EmailAddress)]
        public String correo { get; set; }

        [Display(Name = "Contraseña"), Required(ErrorMessage = "Campo Obligatorio"), DataType(DataType.Password)]
        public String contraseña { get; set; }

        [Display(Name = "Comparacion de Contraseña"), Required(ErrorMessage = "Campo Obligatorio"), DataType(DataType.Password)]
        [Compare("contraseña", ErrorMessage = "La contraseña no coincide")]
        public String compararContraseña { get; set; }

        public int idRol { get; set; }
        public virtual Rol rol { get; set; }
    }
}