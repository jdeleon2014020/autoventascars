﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AUTOVENTAS_Cars.Models
{
    public class Vehiculo
    {
        [Key]
        public int idVehiculo { get; set; }

        [Display(Name = "Marca"), Required(ErrorMessage = "Campo Obligatorio"), DataType(DataType.Text)]
        public String marca { get; set; }

        [Display(Name = "Modelo"), Required(ErrorMessage = "Campo Obligatorio"), DataType(DataType.Text)]
        public String modelo { get; set; }

        [Display(Name = "Color"), Required(ErrorMessage = "Campo Obligatorio"), DataType(DataType.Text)]
        public String color { get; set; }

        [Display(Name = "Combustible"), Required(ErrorMessage = "Campo Obligatorio"), DataType(DataType.Text)]
        public String combustible { get; set; }

        [Display(Name = "Precio"), Required(ErrorMessage = "Campo Obligatorio")]
        public int precio { get; set; }


        public virtual List<Archivo> foto { get; set; }
        public virtual List<Venta> venta { get; set; }
    }
}