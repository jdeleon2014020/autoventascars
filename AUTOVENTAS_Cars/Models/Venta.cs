﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AUTOVENTAS_Cars.Models
{
    public class Venta
    {
        [Key]
        public int idVenta { get; set; }

        [Display(Name = "Fecha"), Required(ErrorMessage = "Campo Obligatorio"), DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "(0:dd-MM-yyyy)", ApplyFormatInEditMode = true)]
        public DateTime fecha { get; set; }
        public int idUsuario { get; set; }
        public int idVehiculo { get; set; }

        public virtual Usuario usuario { get; set; }
        public virtual Vehiculo vehiculo { get; set; }
    }
}