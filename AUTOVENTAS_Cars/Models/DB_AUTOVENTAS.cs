﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;


namespace AUTOVENTAS_Cars.Models
{
    public partial class DB_AUTOVENTAS : DbContext
    {
        public DB_AUTOVENTAS() : base("name=DB_AUTOVENTAS") { }
        public virtual DbSet<Rol> rol { get; set; }
        public virtual DbSet<Archivo> archivo { get; set; }
        public virtual DbSet<Usuario> usuario { get; set; }
        public virtual DbSet<Vehiculo> vehiculo { get; set; }
        public virtual DbSet<Venta> venta { get; set; }
    }
}

