﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace AUTOVENTAS_Cars.Models
{
    public class Rol
    {
        [Key]
        public int idRol { get; set; }
        [Display(Name = "Nombre"), Required(ErrorMessage = "Campo Obligatorio")]
        public String nombre { get; set; }

        [Display(Name = "Descripción"), Required(ErrorMessage = "Campo Obligatorio")]
        public String descripcion { get; set; }

        public virtual List<Usuario> usuarios { get; set; }


    }
}