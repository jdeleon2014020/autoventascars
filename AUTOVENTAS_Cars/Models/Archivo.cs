﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AUTOVENTAS_Cars.Models
{
    public class Archivo
    {
        [Key]
        public int idArchivo { get; set; }
        public String nombre { get; set; }
        public String contentType { get; set; }
        public virtual FileType tipo { get; set; }
        public byte[] contenido { get; set; }

        public int? idVehiculo { get; set; }
        public virtual Vehiculo vehiculo { get; set; }
    }
}