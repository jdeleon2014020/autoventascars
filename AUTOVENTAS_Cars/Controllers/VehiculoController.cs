﻿using AUTOVENTAS_Cars.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AUTOVENTAS_Cars.Controllers
{
    public class VehiculoController : Controller
    {
        public DB_AUTOVENTAS db = new DB_AUTOVENTAS();
        public ActionResult RegistrarVehiculo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RegistrarVehiculo(Vehiculo vehiculo, HttpPostedFileBase archivo)
        {
            if (ModelState.IsValid)
            {
                if (archivo != null && archivo.ContentLength > 0)
                {
                    var imagen = new Archivo {
                        nombre = System.IO.Path.GetFileName(archivo.FileName),
                        tipo=FileType.Imagen,
                        contentType=archivo.ContentType
                    };

                    using (var reader = new System.IO.BinaryReader(archivo.InputStream))
                    {
                        imagen.contenido = reader.ReadBytes(archivo.ContentLength);
                    }
                    vehiculo.foto = new List<Archivo> { imagen };
                }
                db.vehiculo.Add(vehiculo);
                db.SaveChanges();
                ViewBag.mensaje = "El vehiculo fue registrado exitosamente";
                ModelState.Clear();
            }
            return View();
        }
        public ActionResult ListarVehiculo()
        {
            return View(db.vehiculo.ToList());
        }
        
        public ActionResult EditarVehiculo(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.vehiculo.Include(l => l.foto).SingleOrDefault(l => l.idVehiculo == id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

        [HttpPost, ActionName("EditarVehiculo")]
        [ValidateAntiForgeryToken]
        public ActionResult EditarVehiculo(int? id, HttpPostedFileBase archivo)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var vehiculo = db.vehiculo.Find(id);
            if (TryUpdateModel(vehiculo, "", new string[] { "idVehiculo,marca,modelo,color,combustible,precio" }))
            {
                try
                {
                    if (archivo != null && archivo.ContentLength > 0)
                    {
                        if (vehiculo.foto.Any(f => f.tipo == FileType.Imagen))
                        {
                            db.archivo.Remove(vehiculo.foto.First(f => f.tipo == FileType.Imagen));
                        }
                        var imagen = new Archivo
                        {
                            nombre = System.IO.Path.GetFileName(archivo.FileName),
                            tipo = FileType.Imagen,
                            contentType = archivo.ContentType
                        };
                        using (var reader = new System.IO.BinaryReader(archivo.InputStream))
                        {
                            imagen.contenido = reader.ReadBytes(archivo.ContentLength);
                        }
                        vehiculo.foto = new List<Archivo> { imagen };
                    }
                    db.Entry(vehiculo).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }

            }
            return View(vehiculo);
        }

       
        public ActionResult EliminarVehiculo(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.vehiculo.Find(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

        [HttpPost, ActionName("EliminarVehiculo")]
        [ValidateAntiForgeryToken]
        public ActionResult EliminarVehiculo(int id)
        {
            Vehiculo vehiculo = db.vehiculo.Find(id);
            db.vehiculo.Remove(vehiculo);
            db.SaveChanges();
            return RedirectToAction("ListarVehiculo", "Vehiculo");
        }

        public ActionResult Detalle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = db.vehiculo.Find(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

    }
}