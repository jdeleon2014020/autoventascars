﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AUTOVENTAS_Cars.Models;

namespace AUTOVENTAS_Cars.Controllers
{
    public class ArchivoController : Controller
    {
        private DB_AUTOVENTAS db = new DB_AUTOVENTAS();

        public ActionResult Index(int id)
        {
            var ar = db.archivo.Find(id);
            return File(ar.contenido,ar.contentType);
        }
    }
}
