﻿using AUTOVENTAS_Cars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AUTOVENTAS_Cars.Controllers
{
    public class CuentaController : Controller
    {

        public DB_AUTOVENTAS db = new DB_AUTOVENTAS();
        
        // GET: Cuenta
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Usuario usuario)
        {
            var usr = db.usuario.FirstOrDefault(u => u.correo == usuario.correo && u.contraseña==usuario.contraseña);
            if (usr != null)
            {
                Session["nombreUsuario"] = usr.nombre;
                Session["idUsuario"] = usr.idUsuario;
                return VerificarSesion();
            }
            else {
                ModelState.AddModelError("", "Usuario o contraseña incorrecta");
            }
            return View();
        }



        public ActionResult Registrar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registrar(Usuario usuario)
        {
            if (ModelState.IsValid) {
                Rol rol = db.rol.FirstOrDefault(r => r.idRol==2);
                usuario.rol = rol;
                db.usuario.Add(usuario);
                db.SaveChanges();
                ViewBag.mensaje = "El usuario " + usuario + "fue registrado exitosamente";
                ModelState.Clear();
            }
            return View();
        }

        public ActionResult VerificarSesion()
        {
            if (Session["idUsuario"].Equals(2))
            {
                return RedirectToAction("../Home/Index");

            }
            else if(Session["idUsuario"].Equals(1))
            {
                return RedirectToAction("../Home/NoDisponible");
            }
            else {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Logout()
        {
            Session.Remove("IDUsuario");
            Session.Remove("nombreUsuario");
            return RedirectToAction("Login");
        }
    }
}   